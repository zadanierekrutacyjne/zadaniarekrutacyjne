province, register nr, capital, area [km²], population
dolnośląskie 2 Wrocław 19946.70 2904198
kujawsko-pomorskie 4 Bydgoszcz/Toruń 17971.34 2086210
lubelskie 6 Lublin 25122.46 2139726
lubuskie 8 Gorzów_Wielkopolski/Zielona_Góra² 13987.93 1018084
łódzkie 10 Łódź 18218.95 2493603
małopolskie 12 Kraków 15182.79 3372618
mazowieckie 14 Warszawa 35558.47 5349114
wielkopolskie 30 Poznań 29826.50 3475323